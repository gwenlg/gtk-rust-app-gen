use clap::Parser;

#[derive(Parser)]
#[clap(name = "Gtk Rust template gen", author, version, about, long_about = None)]
struct Cli {
	/// Indicate path of the source template
	#[clap(short, long)]
	template: Option<String>,
}

fn main() {
	let cli = Cli::parse();

	match cli.template {
		Some(template) => {
			println!("Generate project from template {}", template);
		}
		None => {
			println!("Can't generate project without template"); //TODO: ask template to user
		}
	}
}
